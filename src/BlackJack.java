import java.util.Scanner;
import java.util.Random;

public class BlackJack {

	public static void main(String[] args) {
		
		// Variables.
		
		Scanner input = new Scanner(System.in);
		Random randomCard = new Random();
		
		String hitOrStay = "";
		String goAgain = "";
		String userName = System.getProperties().getProperty("user.name");
		do {
			
			int player = 0;
			int dealer = 0;
			
			System.out.println("Lets play Blackjack, " + userName + "!");
			
			do {
				player += randomCard.nextInt(13) + 1;
				System.out.println("Your score: " + player);
				
				if (dealer < 16){
					dealer += randomCard.nextInt(13) + 1;
				}
				System.out.println("Dealers score: " + dealer);
				
				// Breaks the loop if the dealer or the player >=21.
				
				if (player >= 21) break;
				if (dealer > 21) break; 
				
				
				System.out.println("Do you want to hit or stay?");
				hitOrStay = input.next();
				
			} while(hitOrStay.equalsIgnoreCase("h"));
			
			//Dealer continues if player wants to stay and the dealer has < 16.
			// Until dealer reaches atleast 16.

			while (dealer < 16 && player <= 21) {
				dealer += randomCard.nextInt(13) + 1;
				System.out.println("Dealers score: " + dealer);
			}

			if ((player < dealer && dealer <= 21) || (player > 21)) {
				System.out.println("Dealer wins.");
			}
			else if (player == dealer) {
				System.out.println("You both draw.");
			}
			else if ((player > dealer && player <= 21) || (dealer > 21)) {
				System.out.println("You win!");
			}

			
			// Asks the user if he/she wants to play again.
			
			System.out.println("Do you want to play again? (y/n)");
			goAgain = input.next();
			
		} while(goAgain.equalsIgnoreCase("y"));
		
		input.close();
		
		
		

	}

}
